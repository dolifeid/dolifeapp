package com.example.hnf.dolifeapp.Activity;

import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.hnf.dolifeapp.Model.StrengthModel;
import com.example.hnf.dolifeapp.Model.User;
import com.example.hnf.dolifeapp.R;
import com.example.hnf.dolifeapp.geocoders.GPSTracker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class RunningActivity extends AppCompatActivity {

    private TextView distance, btnStart, btnStop, startLatLng, stopLatLng, btnSubmit;
    private GPSTracker gps;
    private double startLat = 0, startLng = 0, stopLat = 0, stopLng = 0;
    private float distanceRun;

    private DatabaseReference databaseReferenceUser;
    private DatabaseReference databaseReferenceStr;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_running);

        gps = new GPSTracker(this);
        distance = findViewById(R.id.distance);
        btnStart = findViewById(R.id.btn_start_run);
        btnStop = findViewById(R.id.btn_stop_run);
        startLatLng = findViewById(R.id.start_latLng);
        stopLatLng = findViewById(R.id.end_latLng);
        btnSubmit = findViewById(R.id.btn_submit_runningp);

        user = FirebaseAuth.getInstance().getCurrentUser();
        databaseReferenceUser = FirebaseDatabase.getInstance().getReference("Users");
        databaseReferenceStr = FirebaseDatabase.getInstance().getReference("Strength");

        distance.setText("No Distance");

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gps = new GPSTracker(RunningActivity.this);
                distance.setText("0 M");
                startLat = gps.getLatitude();
                startLng = gps.getLongitude();
                startLatLng.setText(startLat + "," + startLng);
                stopLatLng.setText("");

            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gps = new GPSTracker(RunningActivity.this);
                stopLat = gps.getLatitude();
                stopLng = gps.getLongitude();
//                stopLat = -6.921927;
//                stopLng = 107.607055;
                stopLatLng.setText(stopLat + "," + stopLng);

                Location loc1 = new Location("");
                loc1.setLatitude(startLat);
                loc1.setLongitude(startLng);

                Location loc2 = new Location("");
                loc2.setLatitude(stopLat);
                loc2.setLongitude(stopLng);

                distanceRun = loc1.distanceTo(loc2);
                distance.setText(distanceRun + " M");

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Integer exp = getExp(distanceRun);

                databaseReferenceUser.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User userModel = dataSnapshot.getValue(User.class);
                        int updateExp = userModel.getStrengthExp() + exp;

                        if (updateExp > 100) {
                            updateExp = updateExp - 100;
                            userModel.setStrengthExp(updateExp);
                            int updateLevel = userModel.getStrengthLevel() + 1;
                            userModel.setStrengthLevel(updateLevel);
                        } else {
                            userModel.setStrengthExp(updateExp);
                        }
                        databaseReferenceUser.child(user.getUid()).setValue(userModel);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                StrengthModel strengthModel = new StrengthModel("Running", exp, (int) distanceRun, "Meter");
                databaseReferenceStr.child(user.getUid()).push().setValue(strengthModel);
                finish();
            }
        });

    }

    private int getExp(float distance) {
        int exp = 0;

        if (distance > 10) {
            exp = 40;
        } else if (distance > 5 && distance <= 10) {
            exp = 30;
        } else if (distance > 2 && distance <= 5) {
            exp = 20;
        } else if (distance > 0 && distance <= 2) {
            exp = 10;
        } else {
            exp = 0;
        }

        return exp;
    }


}
