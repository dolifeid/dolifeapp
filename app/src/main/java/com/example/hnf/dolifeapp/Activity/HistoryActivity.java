package com.example.hnf.dolifeapp.Activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.hnf.dolifeapp.Fragment.IntellegentFragment;
import com.example.hnf.dolifeapp.Fragment.RecentFragment;
import com.example.hnf.dolifeapp.Fragment.StrengthFragment;
import com.example.hnf.dolifeapp.R;

import java.util.ArrayList;
import java.util.List;

public class HistoryActivity extends AppCompatActivity {

    private TabLayout tabMainLayout;
    private ViewPager viewPagerMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        viewPagerMain = findViewById(R.id.viewPagerMain);
        tabMainLayout = findViewById(R.id.tabMainLayout);

        setupViewPager(viewPagerMain);
        tabMainLayout.setupWithViewPager(viewPagerMain);
    }

    private void setupViewPager(ViewPager viewPager) {
        HistoryActivity.ViewPagerAdapter adapter = new HistoryActivity.ViewPagerAdapter(getSupportFragmentManager());
        //adapter.addFragment(new PicFragment(), "Pic");
        adapter.addFragment(new IntellegentFragment(), "Intellegent");
        adapter.addFragment(new StrengthFragment(), "Strength");
        viewPager.setAdapter(adapter);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        List<Fragment> fragmentList = new ArrayList<>();
        List<String> fragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }
}
