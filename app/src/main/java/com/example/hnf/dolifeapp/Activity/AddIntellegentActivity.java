package com.example.hnf.dolifeapp.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hnf.dolifeapp.Model.BookIntellegent;
import com.example.hnf.dolifeapp.Model.User;
import com.example.hnf.dolifeapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AddIntellegentActivity extends AppCompatActivity {

    private EditText judulBuku, jumlahHalaman, deskripsi, sinopsi;
    private TextView btnSubmit;
    private DatabaseReference databaseReferenceUser;
    private DatabaseReference databaseReferenceInt;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_intellegent);

        judulBuku = findViewById(R.id.judul_buku);
        jumlahHalaman = findViewById(R.id.jumlah_halaman);
        deskripsi = findViewById(R.id.deskripsi);
        sinopsi = findViewById(R.id.sinopsi);
        btnSubmit = findViewById(R.id.btn_submit_book);

        user = FirebaseAuth.getInstance().getCurrentUser();
        databaseReferenceUser = FirebaseDatabase.getInstance().getReference("Users");
        databaseReferenceInt = FirebaseDatabase.getInstance().getReference("intellegent");

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String tztJudulBuku = judulBuku.getText().toString();
                Integer txtJumlahHalaman = Integer.parseInt(jumlahHalaman.getText().toString());
                String txtDeskripsi = deskripsi.getText().toString();
                String txtSinopsi = sinopsi.getText().toString();
                final Integer exp = getExp(txtJumlahHalaman);

                databaseReferenceUser.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User userModel = dataSnapshot.getValue(User.class);
                        int updateExp = userModel.getIntellegentExp() + exp;

                        if (updateExp > 100) {
                            updateExp = updateExp - 100;
                            userModel.setIntellegentExp(updateExp);
                            int updateLevel = userModel.getIntellegentLevel() + 1;
                            userModel.setIntellegentLevel(updateLevel);
                        } else {
                            userModel.setIntellegentExp(updateExp);
                        }
                        databaseReferenceUser.child(user.getUid()).setValue(userModel);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


                BookIntellegent bookIntellegent = new BookIntellegent(tztJudulBuku, txtJumlahHalaman, txtDeskripsi, txtSinopsi, exp);
                databaseReferenceInt.child(user.getUid()).push().setValue(bookIntellegent);
//                Toast.makeText(AddIntellegentActivity.this, userModel.getIntellegentLevel() + "", Toast.LENGTH_SHORT).show();
//                Log.d("aziz", userModel.getIntellegentExp() + "");
                finish();
            }
        });
    }


    private int getExp(int jumlahHalaman) {
        int exp = 0;

        if (jumlahHalaman > 500) {
            exp = 40;
        } else if (jumlahHalaman > 300 && jumlahHalaman <= 500) {
            exp = 30;
        } else if (jumlahHalaman > 100 && jumlahHalaman <= 300) {
            exp = 20;
        } else if (jumlahHalaman > 0 && jumlahHalaman <= 100) {
            exp = 10;
        } else {
            exp = 0;
        }

        return exp;
    }
}
