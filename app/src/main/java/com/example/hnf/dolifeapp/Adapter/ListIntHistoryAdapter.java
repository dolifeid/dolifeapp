package com.example.hnf.dolifeapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hnf.dolifeapp.Model.BookIntellegent;
import com.example.hnf.dolifeapp.R;

import java.util.List;

public class ListIntHistoryAdapter extends RecyclerView.Adapter<ListIntHistoryAdapter.MyViewHolder> {
    private List<BookIntellegent> bookIntellegents;
    private Context context;

    public ListIntHistoryAdapter(List<BookIntellegent> bookIntellegents, Context context) {
        this.bookIntellegents = bookIntellegents;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_list_int_history, parent, false);

        return new ListIntHistoryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BookIntellegent bookIntellegent = bookIntellegents.get(position);
        holder.judulBuku.setText("Judul Buku: " + bookIntellegent.getJudulBuku());
        holder.jumlahHalaman.setText("Jumlah Halaman: " + bookIntellegent.getJumlahHalaman());
        holder.deskripsi.setText("Deskripsi: " + bookIntellegent.getDeskripsi());
        holder.sinopsis.setText("Sinopsis:" + bookIntellegent.getSinopsis());
        holder.exp.setText("Exp: " + bookIntellegent.getExp() + " Poin");
        String value = "hi, ini update baru intellegent ku \n";
        value += "Judul Buku: " + bookIntellegent.getJudulBuku() + "\n";
        value += "Jumlah Halaman: " + bookIntellegent.getJumlahHalaman() + "\n";
        value += "Deskripsi: " + bookIntellegent.getDeskripsi() + "\n";
        value += "Exp: " + bookIntellegent.getExp() + " Poin" + "\n";
        final String finalValue = value;
        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(finalValue) + "\n\nvia dolifeapp");
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });
    }

    @Override
    public int getItemCount() {
        return bookIntellegents.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView judulBuku, jumlahHalaman, deskripsi, sinopsis, exp;
        private ImageView share;

        public MyViewHolder(View itemView) {
            super(itemView);

            judulBuku = itemView.findViewById(R.id.judul_buku);
            jumlahHalaman = itemView.findViewById(R.id.jumlah_halaman_buku);
            deskripsi = itemView.findViewById(R.id.deskripsi_buku);
            sinopsis = itemView.findViewById(R.id.sinopsis_buku);
            exp = itemView.findViewById(R.id.int_exp);
            share = itemView.findViewById(R.id.share);
        }

        @Override
        public void onClick(View view) {

        }
    }

}
