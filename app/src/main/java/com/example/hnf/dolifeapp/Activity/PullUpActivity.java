package com.example.hnf.dolifeapp.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.hnf.dolifeapp.Model.StrengthModel;
import com.example.hnf.dolifeapp.Model.User;
import com.example.hnf.dolifeapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class PullUpActivity extends AppCompatActivity {

    private TextView pullUpCount, btnPull, btnSubmit;
    private Integer count = 0;

    private DatabaseReference databaseReferenceUser;
    private DatabaseReference databaseReferenceStr;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_up);

        pullUpCount = findViewById(R.id.pull_up_count);
        btnPull = findViewById(R.id.btn_pull_up);
        btnSubmit = findViewById(R.id.btn_submit_pull_up);

        user = FirebaseAuth.getInstance().getCurrentUser();
        databaseReferenceUser = FirebaseDatabase.getInstance().getReference("Users");
        databaseReferenceStr = FirebaseDatabase.getInstance().getReference("Strength");

        pullUpCount.setText(count + "");
        btnPull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pullUpCount.setText(++count + "");
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Integer exp = getExp(count);

                databaseReferenceUser.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User userModel = dataSnapshot.getValue(User.class);
                        int updateExp = userModel.getStrengthExp() + exp;

                        if (updateExp > 100) {
                            updateExp = updateExp - 100;
                            userModel.setStrengthExp(updateExp);
                            int updateLevel = userModel.getStrengthLevel() + 1;
                            userModel.setStrengthLevel(updateLevel);
                        } else {
                            userModel.setStrengthExp(updateExp);
                        }
                        databaseReferenceUser.child(user.getUid()).setValue(userModel);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                StrengthModel strengthModel = new StrengthModel("Pull Up", exp, count, "Kali (X)");
                databaseReferenceStr.child(user.getUid()).push().setValue(strengthModel);
                finish();
            }
        });
    }

    private int getExp(int jumlahPullUp) {
        int exp = 0;

        if (jumlahPullUp > 200) {
            exp = 30;
        } else if (jumlahPullUp > 150 && jumlahPullUp <= 200) {
            exp = 20;
        } else if (jumlahPullUp > 50 && jumlahPullUp <= 150) {
            exp = 15;
        } else if (jumlahPullUp > 0 && jumlahPullUp <= 50) {
            exp = 10;
        } else {
            exp = 0;
        }

        return exp;
    }
}
