package com.example.hnf.dolifeapp.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hnf.dolifeapp.Adapter.ListStrHistoryAdapter;
import com.example.hnf.dolifeapp.Model.BookIntellegent;
import com.example.hnf.dolifeapp.Model.StrengthModel;
import com.example.hnf.dolifeapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class StrengthFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<StrengthModel> strengthModelList;
    private ListStrHistoryAdapter adapter;

    private DatabaseReference databaseReference;
    private FirebaseUser user;

    public StrengthFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_strength, container, false);

        recyclerView = view.findViewById(R.id.rv_list_str);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        strengthModelList = new ArrayList<>();

        user = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference("Strength");

        databaseReference.child(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()){
                    StrengthModel book = data.getValue(StrengthModel.class);
                    strengthModelList.add(book);
                }

                adapter = new ListStrHistoryAdapter(strengthModelList, getActivity());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        return view;
    }

}
