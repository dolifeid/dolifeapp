package com.example.hnf.dolifeapp.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.hnf.dolifeapp.Model.StrengthModel;
import com.example.hnf.dolifeapp.Model.User;
import com.example.hnf.dolifeapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class PushUpActivity extends AppCompatActivity {

    private TextView pushUpCount, btnPush, btnSubmit;
    private Integer count = 0;

    private DatabaseReference databaseReferenceUser;
    private DatabaseReference databaseReferenceStr;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push_up);

        pushUpCount = findViewById(R.id.push_up_count);
        btnPush = findViewById(R.id.btn_push_up);
        btnSubmit = findViewById(R.id.btn_submit_push_up);

        user = FirebaseAuth.getInstance().getCurrentUser();
        databaseReferenceUser = FirebaseDatabase.getInstance().getReference("Users");
        databaseReferenceStr = FirebaseDatabase.getInstance().getReference("Strength");

        pushUpCount.setText(count + "");
        btnPush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pushUpCount.setText(++count + "");
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Integer exp = getExp(count);

                databaseReferenceUser.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User userModel = dataSnapshot.getValue(User.class);
                        int updateExp = userModel.getStrengthExp() + exp;

                        if (updateExp > 100) {
                            updateExp = updateExp - 100;
                            userModel.setStrengthExp(updateExp);
                            int updateLevel = userModel.getStrengthLevel() + 1;
                            userModel.setStrengthLevel(updateLevel);
                        } else {
                            userModel.setStrengthExp(updateExp);
                        }
                        databaseReferenceUser.child(user.getUid()).setValue(userModel);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                StrengthModel strengthModel = new StrengthModel("Push Up", exp, count, "Kali (X)");
                databaseReferenceStr.child(user.getUid()).push().setValue(strengthModel);
                finish();
            }
        });
    }


    private int getExp(int jumlahPushUp) {
        int exp = 0;

        if (jumlahPushUp > 200) {
            exp = 30;
        } else if (jumlahPushUp > 150 && jumlahPushUp <= 200) {
            exp = 15;
        } else if (jumlahPushUp > 50 && jumlahPushUp <= 150) {
            exp = 10;
        } else if (jumlahPushUp > 0 && jumlahPushUp <= 50) {
            exp = 5;
        } else {
            exp = 0;
        }

        return exp;
    }
}
