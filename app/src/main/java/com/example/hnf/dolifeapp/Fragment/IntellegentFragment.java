package com.example.hnf.dolifeapp.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hnf.dolifeapp.Adapter.ListIntHistoryAdapter;
import com.example.hnf.dolifeapp.Model.BookIntellegent;
import com.example.hnf.dolifeapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class IntellegentFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<BookIntellegent> bookIntellegents;
    private ListIntHistoryAdapter adapter;

    private DatabaseReference databaseReference;
    private FirebaseUser user;


    public IntellegentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_intellegent, container, false);

        recyclerView = view.findViewById(R.id.rv_list_int);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        bookIntellegents = new ArrayList<>();

        user = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference("intellegent");

        databaseReference.child(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()){
                    BookIntellegent book = data.getValue(BookIntellegent.class);
                    bookIntellegents.add(book);
                }

                adapter = new ListIntHistoryAdapter(bookIntellegents, getActivity());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        return view;
    }

}
