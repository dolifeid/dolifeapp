package com.example.hnf.dolifeapp.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.example.hnf.dolifeapp.R;

public class StrengthMenuActivity extends AppCompatActivity {

    private LinearLayout btnPushUp, btnPullUp, btnRunning, btnSitUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_strength_menu);

        btnPushUp = findViewById(R.id.view_push_up);
        btnPullUp = findViewById(R.id.view_pull_up);
        btnRunning = findViewById(R.id.view_running);
        btnSitUp = findViewById(R.id.view_sit_uo);

        btnPushUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(StrengthMenuActivity.this, PushUpActivity.class));
            }
        });

        btnPullUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(StrengthMenuActivity.this, PullUpActivity.class));
            }
        });

        btnRunning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(StrengthMenuActivity.this, RunningActivity.class));
            }
        });

        btnSitUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(StrengthMenuActivity.this, SitUpActivity.class));
            }
        });

    }
}
