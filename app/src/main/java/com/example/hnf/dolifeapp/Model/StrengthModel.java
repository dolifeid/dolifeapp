package com.example.hnf.dolifeapp.Model;

public class StrengthModel {
    private String strngType;
    private Integer exp;
    private Integer score;
    private String satuan;

    public StrengthModel(String strngType, Integer exp, Integer score, String satuan) {
        this.strngType = strngType;
        this.exp = exp;
        this.score = score;
        this.satuan = satuan;
    }

    public StrengthModel() {
    }

    public String getStrngType() {
        return strngType;
    }

    public void setStrngType(String strngType) {
        this.strngType = strngType;
    }

    public Integer getExp() {
        return exp;
    }

    public void setExp(Integer exp) {
        this.exp = exp;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }
}
