package com.example.hnf.dolifeapp.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hnf.dolifeapp.Model.BookIntellegent;
import com.example.hnf.dolifeapp.Model.StrengthModel;
import com.example.hnf.dolifeapp.R;

import java.util.List;

public class ListStrHistoryAdapter extends RecyclerView.Adapter<ListStrHistoryAdapter.MyViewHolder>  {
    private List<StrengthModel> strengthModelList;
    private Context context;

    public ListStrHistoryAdapter(List<StrengthModel> strengthModelList, Context context) {
        this.strengthModelList = strengthModelList;
        this.context = context;
    }

    @Override
    public ListStrHistoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_list_str_history, parent, false);

        return new ListStrHistoryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        StrengthModel strengthModel = strengthModelList.get(position);
        if (strengthModel.getStrngType().equalsIgnoreCase("Push Up")){
            holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_push_up));
        } else if (strengthModel.getStrngType().equalsIgnoreCase("Pull Up")){
            holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_pull_up));
        } else if (strengthModel.getStrngType().equalsIgnoreCase("Sit Up")){
            holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_sit_up));
        } else if (strengthModel.getStrngType().equalsIgnoreCase("Running")){
            holder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_run));
        }

        holder.jenisStr.setText("Jenis Strengt: " + strengthModel.getStrngType());
        holder.score.setText("Score: " + strengthModel.getScore() + " " + strengthModel.getSatuan());
        holder.score.setText("Exp: " + strengthModel.getExp() + " Poin");
        String value = "hi, ini update baru strength ku \n";
        value += "Jenis Strengt: " + strengthModel.getStrngType() + "\n";
        value += "Score: " + strengthModel.getScore() + " " + strengthModel.getSatuan() + "\n";
        value += "Exp: " + strengthModel.getExp() + " Poin" + "\n";
        final String finalValue = value;
        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(finalValue) + "\n\nvia dolifeapp");
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });
    }


    @Override
    public int getItemCount() {
        return strengthModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView jenisStr, score, expStr;
        private ImageView icon, share;

        public MyViewHolder(View itemView) {
            super(itemView);

            jenisStr = itemView.findViewById(R.id.jenis_str);
            score = itemView.findViewById(R.id.score);
            icon = itemView.findViewById(R.id.ic_jenis);
            share = itemView.findViewById(R.id.share);

        }

        @Override
        public void onClick(View view) {

        }
    }
}
