package com.example.hnf.dolifeapp.Model;

public class User {
    private String email;
    private String username;
    private String photoUrl;
    private Integer strengthExp;
    private Integer strengthLevel;
    private Integer intellegentExp;
    private Integer intellegentLevel;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Integer getStrengthExp() {
        return strengthExp;
    }

    public void setStrengthExp(Integer strengthExp) {
        this.strengthExp = strengthExp;
    }

    public Integer getStrengthLevel() {
        return strengthLevel;
    }

    public void setStrengthLevel(Integer strengthLevel) {
        this.strengthLevel = strengthLevel;
    }

    public Integer getIntellegentExp() {
        return intellegentExp;
    }

    public void setIntellegentExp(Integer intellegentExp) {
        this.intellegentExp = intellegentExp;
    }

    public Integer getIntellegentLevel() {
        return intellegentLevel;
    }

    public void setIntellegentLevel(Integer intellegentLevel) {
        this.intellegentLevel = intellegentLevel;
    }
}
