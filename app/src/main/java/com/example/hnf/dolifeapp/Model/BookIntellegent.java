package com.example.hnf.dolifeapp.Model;

public class BookIntellegent {
    private String judulBuku;
    private Integer jumlahHalaman;
    private String deskripsi;
    private String sinopsis;
    private Integer exp;

    public BookIntellegent(String judulBuku, Integer jumlahHalaman, String deskripsi, String sinopsis, Integer exp) {
        this.judulBuku = judulBuku;
        this.jumlahHalaman = jumlahHalaman;
        this.deskripsi = deskripsi;
        this.sinopsis = sinopsis;
        this.exp = exp;
    }

    public BookIntellegent() {

    }

    public String getJudulBuku() {
        return judulBuku;
    }

    public void setJudulBuku(String judulBuku) {
        this.judulBuku = judulBuku;
    }

    public Integer getJumlahHalaman() {
        return jumlahHalaman;
    }

    public void setJumlahHalaman(Integer jumlahHalaman) {
        this.jumlahHalaman = jumlahHalaman;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public Integer getExp() {
        return exp;
    }

    public void setExp(Integer exp) {
        this.exp = exp;
    }
}
