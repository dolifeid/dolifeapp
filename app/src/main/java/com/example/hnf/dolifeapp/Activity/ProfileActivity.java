package com.example.hnf.dolifeapp.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hnf.dolifeapp.Model.Post;
import com.example.hnf.dolifeapp.Model.User;
import com.example.hnf.dolifeapp.R;
import com.github.lzyzsd.circleprogress.ArcProgress;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.IOException;

public class ProfileActivity extends AppCompatActivity {

    private static final int PICK_IMAGE_REQUEST = 234;

    private ArcProgress intellegentExp, strngthExp;
    private TextView intellegentLevel, strngthLevel, username, email;
    private DatabaseReference databaseReference;
    private DatabaseReference databaseReferenceUser;
    private FirebaseUser user;
    ;
    private ImageView photo;

    private StorageReference storageReference;
    private Uri filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        intellegentExp = (ArcProgress) findViewById(R.id.intellegent_exp);
        intellegentLevel = (TextView) findViewById(R.id.intellegent_level);
        strngthExp = (ArcProgress) findViewById(R.id.strength_exp);
        strngthLevel = (TextView) findViewById(R.id.strength_level);
        username = (TextView) findViewById(R.id.username);
        email = (TextView) findViewById(R.id.email);
        photo = (ImageView) findViewById(R.id.foto_profil);

        storageReference = FirebaseStorage.getInstance().getReference();
        databaseReferenceUser = FirebaseDatabase.getInstance().getReference("Users");

        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                        PICK_IMAGE_REQUEST);
            }
        });

        user = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference("Users");
        databaseReference.child(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                intellegentExp.setProgress(user.getIntellegentExp());
                intellegentLevel.setText("Level " + user.getIntellegentLevel() + "");
                strngthExp.setProgress(user.getStrengthExp());
                strngthLevel.setText("Level " + user.getStrengthLevel() + "");
                username.setText(user.getUsername());
                email.setText(user.getEmail());


                if (!user.getPhotoUrl().isEmpty()){
                    Picasso.get().load(user.getPhotoUrl())
                            .placeholder(R.drawable.profile_placeholder)
                            .error(R.drawable.profile_placeholder)
                            .into(photo);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    //handling the image chooser activity result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                photo.setImageBitmap(bitmap);

                uploadPost();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void uploadPost() {
        if (filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(ProfileActivity.this);
            progressDialog.setTitle("Upload Foto");
            progressDialog.show();

            // storage
            StorageReference riversRef = storageReference.child("image").child(filePath.getLastPathSegment());
            riversRef.putFile(filePath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    final Uri downloadUri = taskSnapshot.getDownloadUrl();

                    databaseReferenceUser.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            User userModel = dataSnapshot.getValue(User.class);

                            progressDialog.dismiss();
                            userModel.setPhotoUrl(downloadUri.toString());

                            databaseReferenceUser.child(user.getUid()).setValue(userModel);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                    Toast.makeText(getApplicationContext(),
                            "File uploaded!", Toast.LENGTH_SHORT).show();


                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();

                            Toast.makeText(getApplicationContext(),
                                    e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.setMessage("Uploading...");
                        }
                    });

            progressDialog.dismiss();
        } else {
            Toast.makeText(getApplicationContext(), "No files!", Toast.LENGTH_SHORT).show();
        }
    }
}
